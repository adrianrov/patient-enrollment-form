const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');
const Forms = require('@tailwindcss/forms');

module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.warmGray,
        moss: {
          100: 'var(--color-moss-100)',
          200: 'var(--color-moss-200)',
          300: 'var(--color-moss-300)',
          400: 'var(--color-moss-400)',
          500: 'var(--color-moss-500)',
          600: 'var(--color-moss-600)',
          700: 'var(--color-moss-700)',
          800: 'var(--color-moss-800)',
          900: 'var(--color-moss-900)',
        },
        primary: ' var(--color-primary)',
      },
      fontFamily: {
        sans: ['Montserrat', ...defaultTheme.fontFamily.sans],
        serif: ['Mulish', ...defaultTheme.fontFamily.serif],
      },
      fontSize: {
        h1: '4.375rem',
        h2: '1.875rem',
        h3: '1.25rem',
      },
      lineHeight: {
        'extra-tight': ' 1.1',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [Forms],
};

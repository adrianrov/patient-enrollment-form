import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';
import { fonts } from '../constants/global';
import Font from '../components/common/Font';

export default class MyDocument extends Document {
  render(): JSX.Element {
    return (
      <Html>
        <Head>
          <meta charSet="utf-8" />

          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
          {fonts.map((href: string) => {
            return <Font key={`font-${href}`} href={href} />;
          })}
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

import React from 'react';
import Container from '../components/common/Container';
import Footer from '../components/home/Footer';
import Main from '../components/home/Main';
import Metadata from '../components/home/Metadata';

export default function Home(): JSX.Element {
  return (
    <Container>
      <Metadata />
      <Main />
      <Footer />
    </Container>
  );
}

import React, { forwardRef, ForwardedRef } from 'react';
import clsx from 'clsx';

export interface InputProps extends React.HTMLProps<HTMLInputElement> {
  className?: string;
  id: string;
  inputClassName?: string;
  label: string;
  labelClassName?: string;
}

const Input = (
  { className, id, inputClassName, label, labelClassName, ...props }: InputProps,
  ref: ForwardedRef<HTMLInputElement>,
): JSX.Element => {
  const classes = clsx('mb-4', className);
  const inputClasses = clsx(
    'w-full mt-3 text-sm form-input md:text-base placeholder-gray rounded-md border-gray-300',
    inputClassName,
  );
  const labelClasses = clsx('font-semibold text-primary', labelClassName);

  return (
    <div className={classes}>
      <label className={labelClasses} htmlFor={id}>
        {label}
      </label>
      <input ref={ref} className={inputClasses} id={id} {...props} />
    </div>
  );
};

export default forwardRef(Input);

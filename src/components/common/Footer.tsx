import React from 'react';
import clsx from 'clsx';

export default function Footer({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}): JSX.Element {
  const classes = clsx('w-full py-2 flex align-center justify-center', className);

  return <footer className={classes}>{children}</footer>;
}

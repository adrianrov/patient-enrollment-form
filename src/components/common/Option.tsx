import React from 'react';

export interface OptionProps extends React.HTMLProps<HTMLOptionElement> {
  text: string;
  value: string;
}

export default function Option({ text, value, ...props }: OptionProps): JSX.Element {
  return (
    <option value={value} {...props}>
      {text}
    </option>
  );
}

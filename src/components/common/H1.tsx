import React from 'react';
import clsx from 'clsx';

export default function H1({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}): JSX.Element {
  const classes = clsx(
    'font-serif font-semibold text-primary text-3xl lg:text-h1 leading-extra-tight',
    className,
  );

  return <h1 className={classes}>{children}</h1>;
}

import React from 'react';
import clsx from 'clsx';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string;
  children?: React.ReactNode;
}

export default function Button({ children, className, ...props }: ButtonProps): JSX.Element {
  const classes = clsx(
    'bg-moss-500 hover:bg-moss-600 text-white text-base px-10 rounded-md h-12',
    className,
  );

  return (
    <button className={classes} {...props}>
      {children}
    </button>
  );
}

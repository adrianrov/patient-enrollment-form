import React from 'react';
import clsx from 'clsx';

export default function H2({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}): JSX.Element {
  const classes = clsx(
    'font-serif font-semibold text-primary text-2xl lg:text-h2 leading-extra-tight',
    className,
  );

  return <h2 className={classes}>{children}</h2>;
}

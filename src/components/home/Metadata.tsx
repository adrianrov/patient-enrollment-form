import React from 'react';
import Head from 'next/head';

export default function Metadata(): JSX.Element {
  return (
    <Head>
      <title>Patient Enrollment Form</title>
      <meta name="description" content="Parsley Health Patient Enrollment Form" />
    </Head>
  );
}

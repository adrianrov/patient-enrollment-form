import React from 'react';
import clsx from 'clsx';
import CommonFooter from '../common/Footer';

export default function Footer({ className }: { className?: string }): JSX.Element {
  const classes = clsx('bg-gray-200 border-t text-primary border-gray-300', className);

  return (
    <CommonFooter className={classes}>
      <a href="https://www.linkedin.com/in/adrianrov/" target="_blank" rel="noopener noreferrer">
        Created by Adrian Rodriguez
      </a>
    </CommonFooter>
  );
}

import React from 'react';
import clsx from 'clsx';
import H1 from '../common/H1';
import H2 from '../common/H2';
import Form from './Form';

export default function Main({ className }: { className?: string }): JSX.Element {
  const classes = clsx(
    'py-20 px-4 lg:px-10 flex flex-col flex-1 algin-center justify-center max-w-7xl',
    className,
  );

  return (
    <main className={classes}>
      <H1 className="mb-2 lg:mb-4">Patient Enrollment Form</H1>
      <H2 className="mb-4 lg:px-2">Please Provide Us With The Following Information</H2>
      <Form />
    </main>
  );
}

import React, { useRef, MouseEvent } from 'react';
import clsx from 'clsx';
import { genders, maritalStatus, states } from '../../../constants/form';
import Input from '../../common/Input';
import Select from '../../common/Select';
import Section from './Section';

export interface DemographicsData {
  dateOfBirth?: string;
  email?: string;
  firstName?: string;
  gender?: string;
  lastName?: string;
  maritalStatus?: string;
  phone?: string;
  state?: string;
  city?: string;
  zip?: string;
  addressLine1?: string;
  addressLine2?: string;
}

export interface DemographicsProps {
  className?: string;
  data: DemographicsData;
  isActive?: boolean;
  isFirst?: boolean;
  isLast?: boolean;
  setData(data: DemographicsData): void;
  goBack(): void;
  goForward(): void;
}

export default function Demographics({
  className,
  goBack,
  goForward,
  setData,
  isActive,
  isFirst,
  isLast,
}: DemographicsProps): JSX.Element {
  const classes = clsx('flex flex-wrap px-2 py-4', className);
  const firstNameRef = useRef<HTMLInputElement>(null);
  const lastNameRef = useRef<HTMLInputElement>(null);
  const birthdayRef = useRef<HTMLInputElement>(null);
  const genderRef = useRef<HTMLSelectElement>(null);
  const maritalStatusRef = useRef<HTMLSelectElement>(null);
  const phoneRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const stateRef = useRef<HTMLSelectElement>(null);
  const cityRef = useRef<HTMLInputElement>(null);
  const zipCodeRef = useRef<HTMLInputElement>(null);
  const address1Ref = useRef<HTMLInputElement>(null);
  const address2Ref = useRef<HTMLInputElement>(null);
  const references = [
    firstNameRef,
    lastNameRef,
    birthdayRef,
    genderRef,
    maritalStatusRef,
    phoneRef,
    emailRef,
    stateRef,
  ];
  const onNextClick = (event: MouseEvent<HTMLButtonElement>): void => {
    if (references.some((ref) => !ref.current?.value && ref.current?.required)) {
      return;
    }

    event.preventDefault();
    event.stopPropagation();

    const data: DemographicsData = {
      firstName: firstNameRef.current?.value || '',
      lastName: lastNameRef.current?.value || '',
      dateOfBirth: birthdayRef.current?.value || '',
      gender: genderRef.current?.value || '',
      maritalStatus: maritalStatusRef.current?.value || '',
      phone: phoneRef.current?.value || '',
      email: emailRef.current?.value || '',
      state: stateRef.current?.value || '',
      city: cityRef.current?.value || '',
      zip: zipCodeRef.current?.value || '',
      addressLine1: address1Ref.current?.value || '',
      addressLine2: address2Ref.current?.value || '',
    };

    setData(data);
    goForward();
  };
  const onPrevClick = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    goBack();
  };

  return (
    <Section
      className={classes}
      title="Personal Information"
      isActive={isActive}
      isFirst={isFirst}
      isLast={isLast}
      onNextClick={onNextClick}
      onPrevClick={onPrevClick}
    >
      <Input
        ref={firstNameRef}
        label="First Name"
        className="w-full lg:w-1/2 lg:pr-2"
        placeholder="First Name"
        type="text"
        name="firstName"
        id="firstName"
        size={40}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={lastNameRef}
        label="Last Name"
        className="w-full lg:w-1/2 lg:pl-2"
        placeholder="Last Name"
        type="text"
        name="lastName"
        id="lastName"
        size={40}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={birthdayRef}
        label="Date of Birth"
        className="w-full lg:w-1/3 lg:pr-2"
        type="date"
        name="birthday"
        id="birthday"
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Select
        ref={genderRef}
        label="Gender"
        className="w-full lg:w-1/3 lg:px-2"
        id="gender"
        name="gender"
        options={genders}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Select
        ref={maritalStatusRef}
        label="Marital Status"
        className="w-full lg:w-1/3 lg:pl-2"
        id="maritalStatus"
        name="maritalStatus"
        options={maritalStatus}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={lastNameRef}
        label="Email"
        className="w-full lg:w-1/2 lg:pr-2"
        placeholder="email@address.com"
        type="email"
        name="email"
        id="email"
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={lastNameRef}
        label="Phone Number"
        className="w-full lg:w-1/2 lg:pl-2"
        placeholder="+1 (333) 5555-5555"
        type="tel"
        name="phone"
        id="phone"
        size={40}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Select
        ref={stateRef}
        label="State"
        className="w-full lg:w-1/3 lg:pr-2"
        id="state"
        name="state"
        options={states}
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={cityRef}
        label="City"
        className="w-full lg:w-1/3 lg:px-2"
        placeholder="City"
        type="text"
        name="city"
        id="city"
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={zipCodeRef}
        label="Zip Code"
        className="w-full lg:w-1/3 lg:pl-2"
        placeholder="00000"
        type="text"
        name="zip"
        id="zip"
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={address1Ref}
        label="Address Line 1"
        className="w-full"
        placeholder="Street, Avenue"
        type="text"
        name="address1"
        id="address1"
        aria-required="true"
        aria-invalid="false"
        required
      />
      <Input
        ref={address2Ref}
        label="Address Line 2"
        className="w-full"
        placeholder="Apartment number"
        type="text"
        name="address2"
        id="address2"
        aria-required="true"
        aria-invalid="false"
      />
    </Section>
  );
}

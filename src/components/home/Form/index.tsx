import React, { useState } from 'react';
import clsx from 'clsx';
import Demographics, { DemographicsData } from './Demographics';
import { conditions } from '../../../constants/form';
import Conditions, { Condition, ConditionsData } from './Conditions';

export interface FormProps {
  className?: string;
  conditionList?: Condition[];
}

const defaultProps: FormProps = {
  className: '',
  conditionList: conditions,
};

export default function Form({ className, conditionList }: FormProps): JSX.Element {
  const classes = clsx('bg-white p-4 shadow rounded-md', className);
  const [demographics, setDemographics] = useState<DemographicsData>({});
  const [conditions, setConditions] = useState<ConditionsData>({});
  const [currentSection, setCurrentSection] = useState(0);
  const lastSection = 1;
  const goBack = () => setCurrentSection(currentSection < 1 ? 0 : currentSection - 1);
  const goForward = () =>
    setCurrentSection(currentSection >= lastSection ? lastSection : currentSection + 1);

  return (
    <form id="patientEnrollment" className={classes}>
      <Demographics
        data={demographics}
        setData={setDemographics}
        isFirst
        isActive={currentSection === 0}
        goBack={goBack}
        goForward={goForward}
      />
      <Conditions
        list={conditionList}
        data={conditions}
        setData={setConditions}
        isActive={currentSection === 1}
        goBack={goBack}
        goForward={goForward}
      />
    </form>
  );
}

Form.defaultProps = defaultProps;

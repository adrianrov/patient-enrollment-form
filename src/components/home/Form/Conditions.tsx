import React, { useRef, useState, MouseEvent } from 'react';
import clsx from 'clsx';
import Section from './Section';
import Input from '../../common/Input';
import Select from '../../common/Select';
import { OptionProps } from '../../common/Option';

export interface Condition {
  condition: string;
  type: string;
}

export interface ConditionsData {
  conditions?: Condition[];
}

export interface ConditionsProps {
  className?: string;
  data: ConditionsData;
  isActive?: boolean;
  isFirst?: boolean;
  isLast?: boolean;
  list?: Condition[];
  setData(data: ConditionsData): void;
  goBack(): void;
  goForward(): void;
}

const defaultProps = {
  className: '',
  isActive: false,
  isFirst: false,
  isLast: false,
  list: [],
};

const capitalize = (text: string): string => text.charAt(0).toUpperCase() + text.slice(1);

const getOptions = (options: string[]): OptionProps[] =>
  options.map((conditionType: string) => ({
    text: capitalize(conditionType),
    value: conditionType,
  }));

const getTypeList = (conditions: Condition[]): OptionProps[] => {
  const conditionTypes: string[] = [];

  conditions?.forEach((condition: Condition) => {
    if (conditionTypes.indexOf(condition.type) < 0) {
      conditionTypes.push(condition.type);
    }
  });

  return getOptions(conditionTypes);
};

const getConditionList = (conditions: Condition[], type?: string): OptionProps[] => {
  const conditionOptions: string[] = [];

  conditions?.forEach((condition: Condition) => {
    if ((!type || type === condition.type) && conditionOptions.indexOf(condition.condition) < 0) {
      conditionOptions.push(condition.condition);
    }
  });

  return getOptions(conditionOptions);
};

export default function Conditions({
  className,
  goBack,
  goForward,
  isActive,
  isFirst,
  isLast,
  list = [],
  setData,
}: ConditionsProps): JSX.Element {
  const classes = clsx('flex flex-wrap px-2 py-4', className);
  const searchRef = useRef<HTMLInputElement>(null);
  const typesRef = useRef<HTMLSelectElement>(null);
  const conditionsRef = useRef<HTMLSelectElement>(null);
  const selectedRef = useRef<HTMLSelectElement>(null);
  const onNextClick = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    const data: ConditionsData = {
      conditions: selectedConditions.map((option) => {
        const index = Number(option.value);

        return list[index];
      }),
    };

    setData(data);
    goForward();
  };
  const onPrevClick = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    goBack();
  };
  const [typeOptions] = useState(getTypeList(list));
  const [conditionOptions, setConditionOptions] = useState(getConditionList(list));
  const [selectedConditions, setSelectedConditions] = useState<OptionProps[]>([]);
  const onTypeClick = (event: MouseEvent<HTMLSelectElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    if (searchRef.current) {
      searchRef.current.value = '';
    }

    setConditionOptions(getConditionList(list, typesRef.current?.value));
  };
  const onSearch = (event: MouseEvent<HTMLInputElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    if (typesRef.current) {
      typesRef.current.value = '';
    }

    const search = searchRef.current?.value || '';
    const regex = new RegExp(search, 'gi');
    const results = list.filter((option: Condition) => option.condition.match(regex));
    setConditionOptions(getConditionList(results));
  };
  const onConditionClick = (event: MouseEvent<HTMLSelectElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    const text = conditionsRef.current?.value || '';
    const isSelected =
      selectedConditions.findIndex(
        (option: { text: string; value: string }) => option.text === text,
      ) >= 0;

    if (isSelected) {
      return;
    }

    const value = list.findIndex((condition: Condition) => condition.condition === text).toString();
    const option: OptionProps = {
      text,
      value,
    };

    setSelectedConditions(selectedConditions.concat(option));
  };
  const onSelectedConditionClick = (event: MouseEvent<HTMLSelectElement>): void => {
    event.preventDefault();
    event.stopPropagation();

    const index = selectedRef.current?.selectedIndex || -1;

    if (index > -1) {
      setSelectedConditions([
        ...selectedConditions.slice(0, index - 1),
        ...selectedConditions.slice(index),
      ]);
    }
  };

  return (
    <Section
      className={classes}
      title="Conditions"
      isActive={isActive}
      isFirst={isFirst}
      isLast={isLast}
      onNextClick={onNextClick}
      onPrevClick={onPrevClick}
    >
      <Input
        ref={searchRef}
        label="Search Condition"
        className="w-full"
        placeholder="Search..."
        type="text"
        name="search"
        id="conditionSearch"
        aria-invalid="false"
        onChange={onSearch}
      />
      <Select
        ref={typesRef}
        label="Types"
        className="w-full lg:w-1/3 flex flex-col"
        selectClassName="flex-1 lg:rounded-tr-none lg:rounded-br-none"
        id="types"
        name="types"
        options={typeOptions}
        aria-invalid="false"
        useDefaultValue
        defaultValueText="All"
        multiple
        onClick={onTypeClick}
      />
      <Select
        ref={conditionsRef}
        label="Conditions"
        className="w-full lg:w-1/3 flex flex-col"
        selectClassName="flex-1 lg:rounded-none"
        id="conditions"
        name="conditions"
        options={conditionOptions}
        aria-invalid="false"
        multiple
        onClick={onConditionClick}
      />
      <Select
        ref={selectedRef}
        label="Selected Conditions"
        className="w-full lg:w-1/3 flex flex-col"
        selectClassName="flex-1 lg:rounded-tl-none lg:rounded-bl-none"
        id="conditions"
        name="conditions"
        options={selectedConditions}
        aria-invalid="false"
        multiple
        onClick={onSelectedConditionClick}
      />
    </Section>
  );
}

Conditions.defaultProps = defaultProps;

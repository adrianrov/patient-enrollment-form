module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // An array of glob patterns indicating a set of files for which coverage information
  // should be collected
  collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx,ts,tsx}'],

  // The directory where Jest should output its coverage files
  coverageDirectory: 'tests/coverage',

  // This will be used to configure minimum threshold enforcement for coverage results.
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },

  // An array of file extensions your modules use
  moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx'],

  preset: 'ts-jest',

  // The paths to modules that run some code to configure or set up the testing
  setupFilesAfterEnv: ['<rootDir>/enzyme.config.ts'],

  // The test environment that will be used for testing
  testEnvironment: 'node',

  // The glob patterns Jest uses to detect test files
  testMatch: ['**/__tests__/**/*.ts?(x)', '**/?(*.)+(spec|test).ts?(x)'],

  // An array of regexp pattern strings that are matched against all test paths,
  // matched tests are skipped
  testPathIgnorePatterns: ['\\\\node_modules\\\\'],

  // A map from regular expressions to paths to transformers.
  transform: {
    '^.+\\.css$': '<rootDir>/tests/transformers/postcss',
    '^.+\\.(j|t)s?(x)$': 'ts-jest',
  },

  // An array of regexp pattern strings that are matched against all source file paths,
  // matched files will skip transformation
  transformIgnorePatterns: ['<rootDir>/node_modules/'],

  // Indicates whether each individual test should be reported during the run
  verbose: false,

  // Setup Enzyme
  snapshotSerializers: ['enzyme-to-json/serializer'],
};

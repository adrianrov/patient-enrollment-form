const postcss = require('postcss');
const { readFileSync } = require('fs');

const postcssConfig = {
  sassConfig: {},
  lessConfig: {},
  stylusConfig: {},
  cssLoaderConfig: {},
  sassModuleName: '',
  injectIntoDOM: false,
  plugins: [require('tailwindcss'), require('autoprefixer')],
};

module.exports = {
  process(src, fileName) {
    const render = async () => {
      const fileContent = readFileSync(fileName, { encoding: 'utf-8' });
      const code = await postcss(postcssConfig.plugins)
        .process(fileContent)
        .then(
          ({ css }) => css,
          () => '',
        );

      return code;
    };

    render();

    return '';
  },
};
